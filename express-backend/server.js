const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const user = require('./schemes/User');
const key = require('./root/key');

const app = express();

app.use(require('body-parser').json());

app.use('/login', require('./routes/api/login'))

//error handling
app.use((req, res, err, next) => { console.log(err) });


/*var options = {
    useNewUrlParser: true
};

mongoose.connect('mongodb://user123:user123@ds153947.mlab.com:53947/bank_application', options).
    catch(error => console.log(error));
*/

app.listen(5000, () => console.log('server listen on port 5000'));


